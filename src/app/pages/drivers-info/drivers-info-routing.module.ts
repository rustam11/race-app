import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';


@NgModule({
  imports: [RouterModule.forChild([
    {
      path: 'drivers-table',
      children: [
        {
          path: '',
          loadChildren: () => import('./drivers-table/drivers-table.module').then(m => m.DriversTableModule),
        },
        {
          path: 'driver-detail/:driverName',
          loadChildren: () => import('./drivers-table/driver-detail/driver-detail.module').then(m => m.DriverDetailModule),

        },
        {
          path: 'round-results/:seasonId/:roundId',
          loadChildren: () => import('./drivers-table/round-results/round-results.module').then(m => m.RoundResultsModule),
        }
      ]
    },
  ])],
  exports: [RouterModule]
})
export class DriversInfoRoutingModule {
}
