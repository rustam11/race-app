import {Pilot} from "../driver-detail/pilot.model";


export interface RecesResponseModel {
  Circuit: {};
  Results: RaceModel[]
}

export interface RaceModel {
  Constructor: {
    constructorId: string;
    url: string;
    name: string
    nationality: string
  };
  Driver: Pilot;
  FastestLap: {
    AverageSpeed: {
      speed: number;
      units: string;
    };
    Time: {
      time: string;
    };
    lap: number;
    rank: number;
  };
  Time: {
    millis: string;
    time: string;
  };
  grid: number;
  laps: number;
  number: number;
  points: number;
  position: number;
  positionText: number;
  status: string;
}
