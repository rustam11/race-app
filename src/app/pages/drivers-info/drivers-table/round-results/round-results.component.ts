import { Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Location} from '@angular/common';
import {MatTableDataSource, PageEvent} from "@angular/material";

import {PilotsDataService} from "../../../../services/pilots-data.service";
import {CommonService} from "../../../../services/common.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-round-results',
  templateUrl: './round-results.component.html',
  styleUrls: ['./round-results.component.scss']
})
export class RoundResultsComponent implements OnInit, OnDestroy {
  displayedColumns = ['pos', 'no', 'driver', 'constructor', 'laps', 'grid', 'time', 'status', 'points'];
  dataSource = new MatTableDataSource();
  showTable = false;
  isLoading: boolean;
  tablePageSize: number = 10;
  seasonId: number = null;
  roundId: number = null;
  offset = 0;

  driversSub: Subscription;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private pilotsDataService: PilotsDataService,
    private commonService: CommonService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      if(!paramMap.get('seasonId') || !paramMap.get('roundId')) {
        this.location.back();
        return
      }
      this.isLoading = true;
      this.seasonId = +paramMap.get('seasonId');
      this.roundId = +paramMap.get('roundId');
      this.fetchData();
    })
  }

  fetchData() {
    this.driversSub = this.pilotsDataService.getRoundResults(this.seasonId, this.roundId, this.offset, this.tablePageSize).subscribe(res => {
      this.dataSource.data = res.MRData.RaceTable.Races[0].Results;
      this.isLoading = false;
      this.showTable = true;
    }, error => {
      this.isLoading = false;
      this.commonService.handleError();
    })
  }

  doFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLocaleLowerCase();
  }

  getNext(event: PageEvent) {
    this.tablePageSize = event.pageSize;
    this.offset = event.pageSize * event.pageIndex;
    this.fetchData();
  }

  ngOnDestroy(): void {
    if(this.driversSub) {
      this.driversSub.unsubscribe();
    }
  }
}
