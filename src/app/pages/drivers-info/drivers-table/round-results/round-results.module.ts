import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {FlexLayoutModule} from "@angular/flex-layout";
import {RoundResultsComponent} from "./round-results.component";
import {MaterialModule} from "../../../../material.module";


const routes: Routes = [
  {
    path: '',
    component: RoundResultsComponent
  }
];

@NgModule({
  declarations: [RoundResultsComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    FlexLayoutModule,
    RouterModule.forChild(routes)
  ]
})
export class RoundResultsModule { }
