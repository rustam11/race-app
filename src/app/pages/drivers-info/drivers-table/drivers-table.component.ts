import {Component, OnDestroy, OnInit} from '@angular/core';
import {NgForm} from "@angular/forms";
import { Router} from "@angular/router";
import {MatTableDataSource, PageEvent} from "@angular/material";

import {PilotsDataService} from "../../../services/pilots-data.service";
import {Subscription} from "rxjs";
import {CommonService} from "../../../services/common.service";

@Component({
  selector: 'app-drivers-table',
  templateUrl: './drivers-table.component.html',
  styleUrls: ['./drivers-table.component.scss']
})

export class DriversTableComponent implements OnInit, OnDestroy {

  displayedColumns = ['driver', 'dateOfBirth', 'nationality', 'information'];
  dataSource = new MatTableDataSource();
  offsetSearchBySeason = 0;
  offsetSearchAllPilots = 0;
  tablePageSize: number = 10;
  season: number = null;
  round: number = null;
  sortByYear = false;
  isLoading: boolean;
  showTable = false;

  fetchDriversBySeasonSub: Subscription;
  fetchDriversSub: Subscription;

  constructor(
    private pilotsDataService: PilotsDataService,
    private router: Router,
    public commonService: CommonService
  ) {
  }

  ngOnInit() {}

  loadDriversByYear(form: NgForm) {
    this.isLoading = true;
    this.sortByYear = true;
    this.offsetSearchBySeason = 0;
    this.fetchPilotsBySeason(form, () => {
      this.showTable = true;
    });
  }

  loadAllPilots() {
    this.isLoading = true;
    this.sortByYear = false;
    this.offsetSearchAllPilots = 0;
    this.fetchAllPilots(() => {
      this.showTable = true;
    });
  }


  fetchPilotsBySeason(form: NgForm, complete = () => {
  }) {
    this.fetchDriversBySeasonSub = this.pilotsDataService.fetchDriversByYear(this.offsetSearchBySeason, this.tablePageSize, form.value.season, form.value.round).subscribe(response => {
      this.dataSource.data = response.MRData.DriverTable.Drivers;
      this.season = response.MRData.DriverTable.season;
      this.round = response.MRData.DriverTable.round;
      this.isLoading = false;
      complete();
    }, error => {
      this.isLoading = false;
      this.commonService.handleError();
      complete();
    });
  }

  fetchAllPilots(complete = () => {
  }) {
    this.season = null;
    this.fetchDriversSub = this.pilotsDataService.fetchDrivers(this.offsetSearchAllPilots, this.tablePageSize).subscribe(response => {
      this.dataSource.data = response.MRData.DriverTable.Drivers;
      this.isLoading = false;
      complete();
    }, error => {
      this.isLoading = false;
      this.commonService.handleError();
      complete();
    });
  }

  getNext(event: PageEvent, form) {
    this.tablePageSize = event.pageSize;
    if (this.sortByYear) {
      this.offsetSearchBySeason = event.pageSize * event.pageIndex;
      this.fetchPilotsBySeason(form);
    } else {
      this.offsetSearchAllPilots = event.pageSize * event.pageIndex;
      this.fetchAllPilots();
    }
  }

  doFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLocaleLowerCase();
  }

  openResultsPage(form: NgForm) {
    this.router.navigate([`drivers-info/drivers-table/round-results/${form.value.season}/${form.value.round}`]);
  }

  ngOnDestroy(): void {
    if(this.fetchDriversBySeasonSub) {
      this.fetchDriversBySeasonSub.unsubscribe();
    }
    if(this.fetchDriversSub) {
      this.fetchDriversSub.unsubscribe();
    }
  }
}
