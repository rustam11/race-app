import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {DriverDetailComponent} from "./driver-detail.component";
import {MaterialModule} from "../../../../material.module";
import {FlexLayoutModule} from "@angular/flex-layout";
import {FormsModule} from "@angular/forms";

const routes: Routes = [
  {
    path: '',
    component: DriverDetailComponent
  }
];

@NgModule({
  declarations: [DriverDetailComponent],
  imports: [
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class DriverDetailModule { }
