import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Location} from '@angular/common';

import {Subscription} from "rxjs";
import {PilotsDataService} from "../../../../services/pilots-data.service";
import {Pilot} from "./pilot.model";
import {CommonService} from "../../../../services/common.service";

@Component({
  selector: 'app-driver-detail',
  templateUrl: './driver-detail.component.html',
  styleUrls: ['./driver-detail.component.scss']
})
export class DriverDetailComponent implements OnInit, OnDestroy {
  pilot: Pilot = null;
  isLoading: boolean;

  pilotDetailSub: Subscription;
  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private pilotsDataService: PilotsDataService,
    private commnoService: CommonService
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      if (!paramMap.has('driverName')) {
        this.location.back();
        return;
      }
      this.isLoading = true;
      this.pilotDetailSub = this.pilotsDataService.fetchDriverDetail(paramMap.get('driverName')).subscribe(fetchedDriver => {
        this.pilot = fetchedDriver.MRData.DriverTable.Drivers[0];
        this.isLoading = false;
      }, error => {
        this.isLoading = false;
        this.commnoService.handleError();
      })
    })
  }

  ngOnDestroy(): void {
    if(this.pilotDetailSub) {
      this.pilotDetailSub.unsubscribe();
    }

  }
}
