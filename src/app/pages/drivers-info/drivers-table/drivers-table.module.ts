import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {DriversTableComponent} from "./drivers-table.component";
import {MaterialModule} from "../../../material.module";
import {FormsModule} from "@angular/forms";
import {FlexLayoutModule} from "@angular/flex-layout";


const routes: Routes = [
  {
    path: '',
    component: DriversTableComponent
  }
];

@NgModule({
  declarations: [DriversTableComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    FlexLayoutModule,
    RouterModule.forChild(routes)
  ]
})
export class DriversTableModule { }
