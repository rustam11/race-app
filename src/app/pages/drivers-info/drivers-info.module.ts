import {NgModule} from "@angular/core";
import {DriversInfoRoutingModule} from "./drivers-info-routing.module";

@NgModule({
  imports: [DriversInfoRoutingModule],
  declarations: [
  ],
  entryComponents: [],
})
export class DriversInfoModule {}
