import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {FlexLayoutModule} from "@angular/flex-layout";
import {PageNotFoundComponent} from "./page-not-found.component";
import {MaterialModule} from "../../material.module";

const routes: Routes = [
  {
    path: '',
    component: PageNotFoundComponent
  }
];

@NgModule({
  declarations: [PageNotFoundComponent],
  imports: [
    MaterialModule,
    FlexLayoutModule,
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class PageNotFoundModule { }
