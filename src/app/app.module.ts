import { BrowserModule } from '@angular/platform-browser';

import {HttpClientModule} from "@angular/common/http";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {MaterialModule} from "./material.module";
import { HeaderComponent } from './components/header/header.component';
import {ErrorPopUpComponent} from "./components/error-pop-up/error-pop-up";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ErrorPopUpComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,
    HttpClientModule,
  ],
  entryComponents: [ErrorPopUpComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
