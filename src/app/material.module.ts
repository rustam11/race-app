import {NgModule} from "@angular/core";
import {
  MatButtonModule, MatFormFieldModule, MatInputModule, MatTableModule,
  MatToolbarModule, MatPaginatorModule, MatProgressSpinnerModule, MatCardModule, MatDialogModule
} from "@angular/material";

@NgModule({
  imports: [
    MatButtonModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatDialogModule
  ],
  exports: [
    MatButtonModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatDialogModule
  ],
})

export class MaterialModule {
}
