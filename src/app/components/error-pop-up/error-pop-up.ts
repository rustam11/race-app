import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogData {
  progress: number;
}

@Component({
  selector: 'app-stop-training',
  template:
    `
      <h1> Ошибка!</h1>
      <mat-dialog-content>
        <p>Попробуйте повторить запрос позже</p>
      </mat-dialog-content>
      <mat-dialog-actions style="display: flex; justify-content: flex-end">
        <button mat-button [mat-dialog-close]="true">
          Ok
        </button>
      </mat-dialog-actions>
    `
})

export class ErrorPopUpComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}
}
