import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

import {catchError} from "rxjs/operators";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";
import {Pilot} from "../pages/drivers-info/drivers-table/driver-detail/pilot.model";
import {RecesResponseModel} from "../pages/drivers-info/drivers-table/round-results/race.models";

export interface AuthResponseData {
  MRData: {
    DriverTable: {
      Drivers: Pilot[],
      season?: number,
      round?: number
    }
  }
}

export interface PilotResponseData {
  MRData: {
    DriverTable: {
      Drivers: Pilot[]
    }
  }
}

export interface RaceResultsResponseData {
  MRData: {
    RaceTable: {
      Races: RecesResponseModel[]
    }
  }
}

@Injectable({
  providedIn: 'root'
})
export class PilotsDataService {

  constructor(private http: HttpClient) { }

  fetchDrivers(offsetNum: number, limitNum: number): Observable<AuthResponseData> {
    const url = environment.apiUrl;
    const offset = offsetNum.toString();
    const limit = limitNum.toString();
    return this.http.get<AuthResponseData>(url + `f1/drivers.json`, {params: {offset, limit}}).pipe(catchError(error =>  {
      throw new Error('error in source. Details: ' + error);
    }));
  }

  fetchDriversByYear(offsetNum: number, limitNum: number, season: number, round: number): Observable<AuthResponseData> {
    const url = environment.apiUrl;
    const offset = offsetNum.toString();
    const limit = limitNum.toString();
    return this.http.get<AuthResponseData>(url + `f1/${season}/${round}/drivers.json`, {params: {offset, limit}}).pipe(catchError(error =>  {
      throw new Error('error in source. Details: ' + error);
    }));
  }

  fetchDriverDetail(driverName: string): Observable<PilotResponseData> {
    const url = environment.apiUrl;
    return this.http.get<PilotResponseData>(url + `f1/drivers/${driverName}.json`, ).pipe(catchError(error =>  {
      throw new Error('error in source. Details: ' + error);
    }));
  }

  getRoundResults(season: number, round: number, offsetNum: number, limitNum: number): Observable<RaceResultsResponseData> {
    const url = environment.apiUrl;
    const offset = offsetNum.toString();
    const limit = limitNum.toString();
    return this.http.get<RaceResultsResponseData>(url + `f1/${season}/${round}/results.json`, {params: {offset, limit}}).pipe(catchError(error =>  {
      throw new Error('error in source. Details: ' + error);
    }));
  }
}
