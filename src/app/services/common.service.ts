import { Injectable } from '@angular/core';
import {MatDialog} from "@angular/material";
import {Router} from "@angular/router";

import {ErrorPopUpComponent} from "../components/error-pop-up/error-pop-up";

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private dialog: MatDialog, private router: Router) { }

  handleError() {
    const dialogRef = this.dialog.open(ErrorPopUpComponent);

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.router.navigate(['/']);
      }
    });
  }
}
